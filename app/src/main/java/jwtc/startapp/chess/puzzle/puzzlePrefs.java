package jwtc.startapp.chess.puzzle;

import android.os.Bundle;

import jwtc.startapp.chess.MyPreferenceActivity;
import jwtc.startapp.chess.R;

public class puzzlePrefs extends MyPreferenceActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addPreferencesFromResource(R.xml.puzzleprefs);

    }
}

